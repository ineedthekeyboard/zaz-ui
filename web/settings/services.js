define([
    'settings/globals'
], function (globals) {
    var services = {
        //##template
        users: {
            get: {
                data: {
                    url: globals.REST + '/users'
                },
                me: {
                    url: globals.REST + '/users/#userId#'
                    // url: globals.REST + '/me'
                }
            }
        },
        preferences: {
            get: {
                data: {
                    url: globals.REST + '/preferences/#type#/#key#?userId=#userId#'
                }
            },
            post: {
                url: globals.REST + '/preferences'
            },
            delete: {
                url: globals.REST + '/preferences'
            }
        },
        ioManager: {
            get: {
                data: {
                    url: 'features/ioManager.json'
                }
            }
        },
        docViewer: {
            get: {
                url: 'gadgets/docViewer/docViewer.data.json'
            }
        },
        news: {
            get: {
                data: {
                    url: globals.REST + '/news'
                }
            }
        },
        dataGrid: {
            get: {
                config: {
                    url: 'gadgets/dataGrid/dataGrid.config.json'
                },
                data: {
                    url: 'gadgets/dataGrid/dataGrid.data.json'
                }
            }
        },
        applications: {
            get: {
                config: {
                    url: 'gadgets/applications/applications.config.json'
                },
                data: {
                    url: globals.REST + '/applications'
                },
                app: {
                    url: globals.REST + '/applications/#appnum#'
                }
            },
            post: {
                create: {
                    url: globals.REST + '/applications/create?userId=#userId#&reviewer=#reviewer#&approver=#approver#'
                },
                review: {
                    url: globals.REST + '/applications/#appnum#/review?userId=#userId#'
                },
                approve: {
                    url: globals.REST + '/applications/#appnum#/approve?userId=#userId#'
                }
            },
            delete: {
                url: globals.REST + '/applications?userId=#userId#'
            }
        },
        notifications: {
            get: {
                data: {
                    url: globals.REST + '/notifications?userId=#userId#&viewed=#viewed#'
                }
            }
        }
    };

    if (window.CONFIG.debug) {
        window.services = services;
    }

    return services;
});